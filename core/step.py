import json
import os
import pdal


class Step(object):
    task = None

    def __init__(self):
        pass

    def perform(self):
        raise NotImplementedError("Should have implemented this")


class CreateDemStep(Step):
    def __init__(self, las_list, epsg, out_tif, out_las):
        self.las_list = las_list
        self.epsg = epsg
        self.out_tif = out_tif
        self.out_las = out_las

    def perform(self):

        pipe_list = []
        tags = []
        tiles_per_division = 48
        division = 0

        i = 1
        for las in self.las_list:
            tag1 = "a_" + str(i)
            tag2 = "a2_" + str(i)
            tags.append(tag2)

            pipe_list = self.pipe_command(las, tag1, tag2, self.epsg, pipe_list)

            if i % tiles_per_division == 0 and len(self.las_list) != i:
                division += 1
                temp_las = self.out_las.replace('.las', f'_temp{str(division)}.las')
                pipe_list.append({"type": "filters.merge", "inputs": [tag_a for tag_a in tags]})
                pipe_list.append({"type": "writers.las", "filename": temp_las})
                self.execute_pdal(pipe_list)

                pipe_list = []
                tags = [tag2]
                pipe_list = self.pipe_command(temp_las, tag1, tag2, self.epsg, pipe_list)

                if i > tiles_per_division:
                    temp_las = self.out_las.replace('.las', f'_temp{str(division - 1)}.las')
                    os.remove(temp_las)

            i += 1

        pipe_list.append({"type": "filters.merge", "inputs": [tag_a for tag_a in tags]})
        pipe_list.append({"type": "writers.las", "filename": self.out_las})

        self.execute_pdal(pipe_list)

        if division > 0:
            temp_las = self.out_las.replace('.las', f'_temp{str(division)}.las')
            os.remove(temp_las)

        pipe_list = []
        if self.epsg == 26911:
            pipe_list.append({"type": "readers.las", "filename": self.out_las, "spatialreference": "EPSG:26911"})
        else:
            pipe_list.append({"type": "readers.las", "filename": self.out_las, "spatialreference": "EPSG:26912"})
        pipe_list.append({"type": "writers.gdal", "filename": self.out_tif, "output_type": "idw",
                          "gdaldriver": "GTiff", "window_size": 10, "resolution": 1.0})

        self.execute_pdal(pipe_list)

    def execute_pdal(self, pipe_list):

        pipe_command = {"pipeline": pipe_list}
        pipeline = pdal.Pipeline(json.dumps(pipe_command))
        pipeline.validate()
        pipeline.execute()

    def pipe_command(self, las, tag1, tag2, epsg, pipe_list):

        if epsg == 26911:
            pipe_list.append({"type": "readers.las", "filename": las, "spatialreference": "EPSG:26911",
                              "tag": tag1})
            pipe_list.append({"type": "filters.range", "limits": "Classification[2:2]", "inputs": [tag1],
                              "tag": tag2})
        else:
            pipe_list.append({"type": "readers.las", "filename": las, "spatialreference": "EPSG:26912",
                              "tag": tag1})
            pipe_list.append({"type": "filters.range", "limits": "Classification[2:2]", "inputs": [tag1],
                              "tag": tag2})

        return pipe_list
class Task(object):
    """
    Task should be implemented via a child class. See MultiStepTask as an example
    """
    def __init__(self, step):
        self.step = step
    """
    Performs the actual work for the task; requires overriding.
    """

    def run(self):
        result = self.step.perform()
        print ('...task completed')


class MultiStepTask(Task):
    """
    A specialized version of Task that consists of multiple steps to be performed
    consecutively.
    """

    def __init__(self, global_context, steps):
        super(MultiStepTask, self).__init__(global_context)
        self.steps = steps

        # give the step a reference to the task to help with logging/debugging as
        # well as access to the global_context
        for step in self.steps:
            step.task = self

    def run(self):
        for step_number, step in enumerate(self.steps):
            result = step.perform()
            if not result:
              break

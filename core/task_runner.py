import logging
import time
import traceback
import multiprocessing


class TaskRunner(object):
    """
    Responsible for running tasks in an asynchronous pool. If you provide a size_of_pool
    of 1 then it will run them consecutively.
    """

    def __init__(self, size_of_pool=multiprocessing.cpu_count()):
        self.size_of_pool = size_of_pool
        self.pool = multiprocessing.Pool(processes=size_of_pool)
        self.manager = multiprocessing.Manager()

    def get_manager(self):
        """
        Returns the multiprocessing.Manager instance which
        can be used to construct globally shared objects
        such as locks.
        """
        return self.manager

    def run(self, tasks):
        """
        Runs all of the tasks in the async pool.
        """
        start = time.time()
        # print("......All {num_of_tasks} tasks have been submitted to the pool with {size_of_pool} workers".format(
        #     num_of_tasks=len(tasks), size_of_pool=self.size_of_pool))

        for task in tasks:

            # This will send the tasks to the asynchronous pool:
            self.pool.apply_async(run_task, (task,))
            # If the above is giving you problems where the tasks seemingly just
            # aren't doing ANYTHING it could be because the "apply_async" function is
            # suppressing the exception. Sometimes it can help to temporarily
            # use "apply" instead which doesn't suppress:
            #
            #self.pool.apply(run_task, (task,))


        self.pool.close()
        # wait for tasks to complete
        self.pool.join()

        end = time.time()
        duration = end - start
        print("......All tasks have completed in {duration:.2f} seconds".format(duration=duration))


# Due to some quirks with python 2, we have to configure the
# logging outside the scope of the above class (same goes for the
# run_task function below)
multiprocessing.log_to_stderr(logging.WARN)


# noinspection PyBroadException
def run_task(task):
    """
    Runs a task, catching any exceptions from the task and
    logs it into the primary multiprocessing logger.
    """
    try:
        # print("[{task_name}] Beginning task".format(task_name=task.identifier))
        print('Running a task')
        task.run()
    except Exception:
        multiprocessing.get_logger().error(traceback.format_exc())

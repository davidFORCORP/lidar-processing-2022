from osgeo import ogr

import laspy, pdal, json
from core.task import Task
from core.task_runner import TaskRunner
from core.step import CreateDemStep
import os


class CreateDem:
    def __init__(self):
        pass

    def create_dem_pdal_complete(self, las_files, epsgs, out_be_dem_location, out_las_dem_location):

        tasks = []
        for tile in epsgs:
            lower_tile = tile.lower()
            out_tif = out_be_dem_location + '/be_dem_' + lower_tile + '.tif'
            out_las = out_las_dem_location + '/nts_las_' + lower_tile + '.las'

            data_to_unite = SupportingFunctions().find_nts_las(lower_tile, las_files)

            if not data_to_unite or os.path.isfile(out_tif):
                continue

            task = CreateDemStep(data_to_unite, 26900 + epsgs[tile], out_tif, out_las)

            if not os.path.isfile(out_tif):
                tasks.append(Task(task))

        task_runner = TaskRunner(1)
        task_runner.run(tasks)


class SupportingFunctions():
    def __init__(self):
        pass

    def find_nts_las(self, tile, las_files):

        nts_las = []
        for file in las_files:
            if tile not in file:
                continue
            nts_las.append(file)

        return nts_las

    def write_las(self, array_, las_header, out_las):

        new_file = laspy.create(point_format=las_header.point_format, file_version=las_header.version)
        new_file.points = array_

        new_file.write(out_las)

    def get_shp_field_values_2array(self, in_shp, fields=None):

        driver = ogr.GetDriverByName("ESRI Shapefile")
        dataSource = driver.Open(in_shp, 0)
        layer = dataSource.GetLayer()

        values = []
        for feature in layer:
            if fields:
                if len(fields) == 1:
                    values.append(feature.GetField(fields[0]))
                else:
                    iterator = []
                    for row in fields:
                        iterator.append(feature.GetField(row))
                    values.append(iterator)

        layer.ResetReading()
        values.sort()

        return values

    def create_filtered_shapefile(self, in_shapefile, out_shapefile, filter_):
        # what do you want to keep?

        dataSource = ogr.Open(in_shapefile, 0)
        input_layer = dataSource.GetLayer()

        input_layer.SetAttributeFilter(filter_)
        driver = ogr.GetDriverByName('ESRI Shapefile')
        out_ds = driver.CreateDataSource(out_shapefile)
        out_layer = out_ds.CopyLayer(input_layer, str(999))

        del input_layer, out_layer, out_ds

    def pdal_bare_earth_dem(self, in_las_file, out_tif_file, window, cell_size, terrain_type):

        slope = 0
        if terrain_type == 'LOW':
            slope = 0.8
        elif terrain_type == 'MED':
            slope = 1
        elif terrain_type == 'HIGH':
            slope = 1.2

        create_bare_earth = {
            "pipeline": [
                {
                    "type": "readers.las",
                    "filename": in_las_file
                },
                {
                    "type": "filters.pmf",
                    "cell_size": cell_size,
                    "max_window_size": window,
                    "slope": slope
                },
                {
                    "type": "writers.gdal",
                    "filename": out_tif_file,
                    "resolution": cell_size,
                    "output_type": "idw"
                }
            ]
        }
        pipeline = pdal.Pipeline(json.dumps(create_bare_earth))
        pipeline.validate()
        pipeline.execute()

import os, sys, csv
import numpy as np
from laspy.file import File
from shutil import copyfile

class find_las_and_clean():
    def __init__(self):
        pass

    def copy_all_las_to_new_location(self, copy_location):
        print('getting LAS file duplicates and non-duplicates')
        dataset = '//rasters/data/caribou_ramp/all_files_and_locations.csv'  ##dataset = '//rasters/data/las_file_location_references_20210723.csv'
        first_name = copy_location + '/nts_of48_tile_'

        # list_of_copied_data = [r[2] for r in os.walk(copy_location)]
        list_of_copied_data = []
        for path, directories, files in os.walk(copy_location):

            for file in files:
                list_of_copied_data.append(file.lower())


        # this creates an array of arrays of arrays [[non_duplicate_tile, duplicate_tile]]
        # based on csv vintage, the input csv in the following function and its vintage is static. Files will show up as not copied, but they were already, we are just going over the entire csv right now.
        # anytime we encounter a location where the file already is in existance in the copied data, we CONTINUE process to solve this if the copied file location has the file that would have been created from the process if successfully moved over (copied already)..
        non_duplicate, duplicates = self.duplicate_non_duplicate(dataset, list_of_copied_data)
        # each of the individual non or duplicate are arrays of arrays
        # duplicates = [[[[yyy, zzz], [yyy, zzz]], xxx], [[[yyy, zzz], [yyy, zzz]], xxx]]

        print('Copying of non duplicates')

        counting_iterations_to_discover_corrupt_files = 4
        # i.e., 4 tries at copying before we quit.
        # The files that fail will not be deleted and will have to be reviewed.
        # These are printed if they occur.

        self.copy_the_non_duplicates(non_duplicate, counting_iterations_to_discover_corrupt_files, first_name)

        print('working on the duplicates')
        dead_tiles = []
        tasks_copy_duplicates = []
        # tasks_runner_copy_duplicates = TaskRunner(12)

        # this is just protection against getting fucked by something
        for tiles in duplicates:

            if tiles[1] in (f[0] for f in non_duplicate):
                print(
                    'HOLD ALL HORSES --- WOAHHHHHHH----- WHAT IS HAPPENING HERE. SHOULD NOT BE POSSIBLE - LIKE AT ALLLLLLLLLLLLLLLLLLLLLLLL')
                sys.exit()

        for tiles in duplicates:
            files_to_test = tiles[0]
            tile = tiles[1]

            # target is what we are going to call the product of the final, "mosaiced", dataset
            target = copy_location + '/nts_of48_tile_' + tile.lower() + '.las'

            self.eliminate([files_to_test, target])
        print('..running copy of all duplicates')

    def eliminate(self, duplicate_tiles):
        target = duplicate_tiles[1]
        duplicates = duplicate_tiles[0]
        current_point_count = []
        tiles_to_test = []
        sys.exit()
        point_count = 0
        for file_location in duplicates:

            stats_ = self.las_properties(file_location)
            if stats_[4] in current_point_count:
                continue
            current_point_count.append(stats_[4])

            tiles_to_test.append(file_location)
            point_count += stats_[4]

        if len(tiles_to_test) == 1:
            # if point_count > 1
            copyfile(tiles_to_test[0], target)
        else:
            master_las_array, header_ = self.las_overlap_resolution(tiles_to_test, include_classification=True)
            self.write_las(master_las_array, las_header=header_, out_las=target, include_intensity=True,
                           include_classification=True)

    def copy_the_non_duplicates(self, non_duplicate, counting_iterations_to_discover_corrupt_files, first_name):

        list_of_locations_that_failed_copying_multiple_times = []

        for each_tile in non_duplicate:
            file_location = each_tile[0]
            tile = each_tile[1]

            # what are we gonna call it?
            target = first_name + tile + '.las'

            # prime regimented parameters
            target_file_size = 0
            file_location_file_size = 0
            a = 0
            b = 0

            # The only files that we will be keeping in their original location and in that form are ones that
            # we are stitching together to create a whole.

            try:
                target_file_size = os.path.getsize(target)
                a = 1
                file_location_file_size = os.path.getsize(file_location)
                b = 1
            except:
                pass

            if a == 1 and b == 1 and target_file_size != file_location_file_size and target_file_size < file_location_file_size:
                # likely an interrupted file copy

                if not os.path.isfile(file_location):
                    print(tile)
                    continue

                try:
                    os.remove(target)
                    print('--- a file present when I do n0t think there should be at this junction \n '
                          'unless we are running the same data inputs with the outputs already partially or completely exist')
                except:
                    pass

                # just a method to get a list of things to process at the same time according to the number of cores we are using (i.e., CPU cores)
                # nothing happens here, but the gun is about to get another round in the mag
                copyfile(file_location, target)
                # tasks_copy_non_duplicates.append(Step_Copy(file_location, target))
                list_of_locations_that_failed_copying_multiple_times.append(file_location)

            elif a == 0:
                # not copied yet
                if not os.path.isfile(file_location):
                    print(tile)
                    continue

                try:
                    os.remove(target)
                    print('--- a file present when I do n0t think there should be at this junction \n '
                          'unless we are running the same data inputs with the outputs already partially or completely exist')
                except:
                    pass

                copyfile(file_location, target)

            elif a == 1 and b == 0:
                # we have target, but it looks like the source has already been deleted; no worries
                pass

            elif a == 1 and b == 1 and target_file_size == file_location_file_size:
                pass
                # everything is dandy; safe to delete
            elif a == 1 and b == 1 and target_file_size != file_location_file_size and target_file_size > file_location_file_size:
                # already copied and target is bigger, so skip
                pass
            else:
                print('Some condition I am not accounting for \n file: {file_location}'.format(
                    file_location=file_location))
                sys.exit()

    def duplicate_non_duplicate(self, dataset, list_of_copied_data):
        tiles = []

        duplicates_already = []
        for data_ in list_of_copied_data:
            if len(data_.split('_')) > 3:
                duplicates_already.append(data_.split('_')[3])

        with open(dataset, 'r') as f:
            reader = csv.reader(f)
            next(reader)
            for file_location, file_, basename, tile, file_type in reader:
                if 'pc' not in file_location.lower():  # GoA data always has this
                    continue

                t = False
                tile__ = tile.lower()
                for data_ in list_of_copied_data:

                    if tile__ in data_ or tile__ in duplicates_already:
                        t = True
                        break
                if not t:
                    tiles.append([file_location, tile.lower()])

        # align our tiles to be grouped
        sorted(tiles, key=lambda x: x[1])

        non_duplicate_tile = []
        duplicate_tile = []

        index_a = 0
        future_tile = None

        while index_a < len(tiles):
            file_location, tile = tiles[index_a][0].lower(), tiles[index_a][1].lower()

            if not os.path.isfile(file_location):
                index_a += 1
                continue

            if index_a + 1 >= len(tiles) and future_tile:
                non_duplicate_tile.append(tiles[index_a])
                break

            if index_a + 1 >= len(tiles):
                break

            future_tile = tiles[index_a + 1][1]
            future_file = tiles[index_a + 1][0]    # it's not the end (i.e., last record in array). Proceed..... we have a file that exists in the file folder structure in which it was presented to the formation of the csv.

            if future_tile != tile:
                non_duplicate_tile.append(tiles[index_a])  # the irreffutabbbbbbbble non-duplicate.
                index_a += 1
                future_tile = None
                continue

            duplicate_to_append = [file_location, future_file]
            while future_tile:

                index_a += 1
                future_tile = tiles[index_a][1]
                future_file = tiles[index_a][0]

                if future_tile == tile:
                    duplicate_to_append.append(future_file)

                else:
                    future_tile = None

            duplicate_tile.append([duplicate_to_append, tile])

        return non_duplicate_tile, duplicate_tile

    def las_properties(self, in_las, round_=3):
        # https://pythonhosted.org/laspy/file.html
        inFile = File(in_las, mode="r")
        las_x_min = round(inFile.header.min[0], round_)  # like 597061.84
        las_x_max = round(inFile.header.max[0], round_)
        las_y_min = round(inFile.header.min[1], round_)
        las_y_max = round(inFile.header.max[1], round_)
        point_count = inFile.__len__()

        inFile.close()

        result = [las_x_min, las_x_max, las_y_min, las_y_max, point_count]

        return result

    def las_overlap_resolution(self, overlap_data_array,  include_classification=False):
        # overlap_data_array = [file_location_las, las_tilename]

        header_ = None

        # hopefully, this is big enough
        current_dimensions = 4
        if include_classification:
            current_dimensions += 1

        master_las_array = np.ndarray((1000000000, current_dimensions))

        index_a = 0
        through_1 = False
        nullify_x_y_list = []

        for file_location in overlap_data_array:

            if not header_:
                current_las_data_array, header_ = self.read_las_and_load_to_np(file_location, include_intensity=True, return_header=True, round_xy_to_decimal_places=3, include_classification=include_classification)
            else:
                through_1 = True
                current_las_data_array = self.read_las_and_load_to_np(file_location, include_intensity=True, return_header=False, round_xy_to_decimal_places=3, include_classification=include_classification)
                # intensity [3] before classification [4]

            for each_las_point in current_las_data_array:
                # we may have problems if the data is from different densities with this method
                # - especially against data that is a lot of years old
                # I am not going that far down this rabbit hole.

                if through_1:

                    if include_classification:

                        check_list = [np.round(each_las_point[0], 2), np.round(each_las_point[1], 2), np.round(each_las_point[2], 2), each_las_point[4]]

                    else:

                        check_list = [np.round(each_las_point[0], 2), np.round(each_las_point[1], 2), np.round(each_las_point[2], 2)]

                    # no points to come from other data sources than the first if within 0.01m in 3D space
                    # TODO this will mix LiDAR files currently if not logically
                    #  separated before processing by vintage!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    if check_list in nullify_x_y_list:
                        continue

                master_las_array[index_a] = np.array(each_las_point)

                if include_classification:

                    nullify_x_y_list.append([np.round(current_las_data_array[0], 2), np.round(current_las_data_array[1], 2), np.round(each_las_point[2], 2), ])

                else:

                    nullify_x_y_list.append([np.round(current_las_data_array[0], 2), np.round(current_las_data_array[1], 2), np.round(each_las_point[2], 2)])

                index_a += 1

        master_las_array = master_las_array[:index_a]  # do i need a minus 1 on index_a here?

        return master_las_array, header_

    def read_las_to_make_a_header(self, in_las):
        in_file = File(in_las, mode="r")

        return in_file.header

    def read_las_and_load_to_np(self, in_las, include_intensity=True, return_header=True, get_z_max=False,
                                get_x_y_max_min=False, index_of_additional_fields=None, round_xy_to_decimal_places=4, return_point_count=False, for_dem=False, include_classification=False):
        # z_max is intentionally set to not have rounding

        in_file = File(in_las, mode="r")

        las_header = in_file.header

        original_x = in_file.x.round(round_xy_to_decimal_places)
        original_y = in_file.y.round(round_xy_to_decimal_places)
        original_z = in_file.z

        x_y_max_min = []
        if get_x_y_max_min:
            max_x = np.amax(original_x)
            max_y = np.amax(original_y)
            min_x = np.amin(original_x)
            min_y = np.amin(original_y)
            x_y_max_min = [max_x, max_y, min_x, min_y]

        if round_xy_to_decimal_places == 0:
            original_x = original_x.astype(int)
            original_y = original_y.astype(int)

        main_stack = (original_x, original_y, original_z)

        if include_intensity and include_classification:
            original_i = in_file.Intensity
            original_classification = in_file.Classification

            main_stack = (original_x, original_y, original_z, original_i, original_classification)

        if include_intensity and not include_classification:
            original_i = in_file.Intensity
            main_stack = (original_x, original_y, original_z, original_i)

        if not include_intensity and include_classification:
            original_classification = in_file.Classification
            main_stack = (original_x, original_y, original_z, original_classification)

        if index_of_additional_fields is not None:
            for i, index in enumerate(index_of_additional_fields):

                if i == 0:
                    data1 = [x[0][index] for x in in_file.points]
                    main_stack += data1
                elif i == 1:
                    data2 = [x[0][index] for x in in_file.points]
                    main_stack += data2
                elif i == 2:
                    data3 = [x[0][index] for x in in_file.points]
                    main_stack += data3
                else:
                    print('NEED MORE OF THESE')

        del in_file
        max_z = 40

        if get_z_max:
            max_z = np.amax(original_z)

        # turn to numpy array
        # (1D array -to- row vector -to- column vector)
        array_2d = np.vstack(main_stack).transpose()
        points = array_2d.shape[0]
        del main_stack

        if return_header:
            if get_z_max:
                if get_x_y_max_min:
                    return array_2d, las_header, max_z, x_y_max_min
                else:
                    return array_2d, las_header, max_z
            else:
                if get_x_y_max_min:
                    if return_point_count:
                        return array_2d, las_header, x_y_max_min, points
                    else:
                        return array_2d, las_header, x_y_max_min
                else:
                    if return_point_count:
                        return array_2d, las_header, points
                    else:
                        return array_2d, las_header
        else:
            if get_z_max:
                if get_x_y_max_min:
                    if return_point_count:
                        return array_2d, max_z, x_y_max_min, points
                    else:
                        return array_2d, max_z, x_y_max_min
                else:
                    if return_point_count:
                        return array_2d, max_z, points
                    else:
                        return array_2d, max_z
            else:
                if get_x_y_max_min:
                    if return_point_count:
                        return array_2d, x_y_max_min, points
                    else:
                        return array_2d, x_y_max_min
                else:
                    if return_point_count:
                        return array_2d, points
                    else:
                        return array_2d

    def write_las(self, array_, las_header, out_las, include_intensity=True, VLRs=None, add_colour=None, include_classification=False):

        xx = (array_[:array_.shape[0], :1]).flatten()
        yy = (array_[:array_.shape[0], 1:2]).flatten()
        zz = (array_[:array_.shape[0], 2:3]).flatten()
        ii = []
        classification = []

        if include_intensity and not include_classification:
            ii = (array_[:array_.shape[0], 3:4]).flatten()

        if include_intensity and include_classification:
            ii = (array_[:array_.shape[0], 3:4]).flatten()
            classification = (array_[:array_.shape[0], 4:5]).flatten()

        if not include_intensity and include_classification:
            classification = (array_[:array_.shape[0], 3:4]).flatten()

        min_arr = [np.amin(xx), np.amin(yy), np.amin(zz)]
        max_arr = [np.amax(xx), np.amax(yy), np.amax(zz)]

        out_file = File(out_las, mode='w', header=las_header)

        out_file.x = xx
        out_file.y = yy
        out_file.z = zz

        if include_intensity:
            out_file.intensity = ii

        if include_classification:
            out_file.classification = classification

        out_file.close()

        # add colour?
        if add_colour:
            reds = add_colour[0]
            greens = add_colour[1]
            blues = add_colour[2]
            out_file.red = reds
            out_file.green = greens
            out_file.blue = blues

        # fix the header
        in_file = File(out_las, mode="rw")
        las_header = in_file.header
        las_header.min = min_arr
        las_header.max = max_arr
        ## TODO ADD VLR carrier of max_value instead of returning int from invert_las (only location right now)
        in_file.close()

        return True
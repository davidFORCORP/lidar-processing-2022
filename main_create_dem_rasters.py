import shutil
import sys

from create_dem.create_dem import CreateDem, SupportingFunctions
import os

def main():

    nts = r'//rasters/data/Caribou_RAMP/caribou_working/LiDAR/Processed_Data/brl/brl_unprocessed_lidar_nts.shp'
    alberta_nts = r'//silver/projects/SpatialData_Core/Canada/nts/alberta_nts_50k.shp'
    las_location = r'//rasters/data/Alberta_LiDAR_LAS_Raw_Duplicates_Solved'
    out_be_dem_location = r'//rasters/data/Alberta_LiDAR_Processed/be_dem_by_nts'
    out_las_dem_location = r'//rasters/data/Alberta_LiDAR_Processed/las_dem_by_nts'

    # epsgs = dict([(key.lower(), epsg) for key, epsg in
    #               SupportingFunctions().get_shp_field_values_2array(nts, ['NTS_SNRC'])])  # 'utm'
    epsgs = dict([(key.lower(), 11) for key in
                  SupportingFunctions().get_shp_field_values_2array(nts, ['NTS_SNRC'])])  # 'utm'

    las_files = []
    for root, dirs, files in os.walk(las_location):
        for file_ in files:
            if file_.endswith('.las'):
                las_files.append(os.path.join(root, file_.lower()))

    CreateDem().create_dem_pdal_complete(las_files, epsgs, out_be_dem_location, out_las_dem_location)


if __name__ == "__main__":
    main()

from las_processing.las_find_and_clean import find_las_and_clean

def main():

    find_las_and_clean().copy_all_las_to_new_location('//rasters/data/Alberta_LiDAR_LAS_Raw_Duplicates_Solved')

if __name__ == "__main__":
    main()